public class Movie {
    public static final int CHILDRENS = 2;
    public static final int REGULAR = 0;
    public static final int NEW_RELEASE = 1;
    private String title;
    private int priceCode;
    public Movie(String name, int priceCode) {
        String _name = name;
        setPriceCode(priceCode);
    }
    public int getPriceCode() {
        return priceCode;
    }
    public void setPriceCode(int arg) {
        priceCode = arg;
    }
    public String getTitle (){
        return title;
    };

    double getCharge(int daysRented) {
        return Price.getCharge(daysRented);
    }

    int getFrequentRenterPoints(int daysRented) {
        return Price.getFrequentRenterPoints(daysRented);
    }
}