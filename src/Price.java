abstract class Price {
    static int getPriceCode() {
        return 0;
    }

    static double getCharge(int daysRented) {
        return 0;
    }

    static int getFrequentRenterPoints(int daysRented){
        return 1;
    }
}
class ChildrenPrice extends Price {
    static int getPriceCode() {
        return Movie.CHILDRENS;
    }
    static double getCharge(int daysRented){
        double result = 1.5;
        if (daysRented > 3)
            result += (daysRented - 3) * 1.5;
        return result;
    }
}
class NewReleasePrice extends Price {
    static int getPriceCode() {
        return Movie.NEW_RELEASE;
    }
    static double getCharge(int daysRented){
        return daysRented * 3;
    }
    static int getFrequentRenterPoints(int daysRented) {
        return (daysRented > 1) ? 2: 1;
    }
}
class RegularPrice extends Price {
    static int getPriceCode() {
        return Movie.REGULAR;
    }
    static double getCharge(int daysRented){
        double result = 2;
        if (daysRented > 2)
            result += (daysRented - 2) * 1.5;
        return result;
    }
}